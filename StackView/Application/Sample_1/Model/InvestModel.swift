//
//  InvestModel.swift
//  StackView
//
//  Created by Pheayuth's iMac on 22/8/22.
//

import Foundation

struct InvestModel {
    
    let image           : String?
    let title           : String?
    let subTitle        : String?
    let price           : String?
    let starRatingImg   : String?
    let firstLogoComp   : String?
    let logoComp        : String?
    let nameComp        : String?
    let isNatPosValue   : Bool? // NatPosValue = Nagative/Postive Value (+/-)
}
