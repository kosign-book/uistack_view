//
//  InvestVM.swift
//  StackView
//
//  Created by Pheayuth's iMac on 22/8/22.
//

import Foundation

class InvestVM {
    var investData = [InvestModel]()
    
    func getInvest(){
        investData = [
            InvestModel(image           : "Starbucks",         title    : "Starbuck Coffee",
                        subTitle        : "",          price    : "- $25.00",
                        starRatingImg   : "star1",     firstLogoComp    : "Monzo",
                        logoComp        : "",               nameComp    : "Monzo",          isNatPosValue: true),
            InvestModel(image           : "Gloria_Jeans",      title    : "Gloria Jean",
                        subTitle        : "01:30 PM",          price    : "+ $85.00",
                        starRatingImg   : "",          firstLogoComp    : "Tela",
                        logoComp        : "Tela",           nameComp    : "Leveno",         isNatPosValue: false),
            InvestModel(image           : "Amazon",            title    : "Amazon Coffee",
                        subTitle        : "06:00 AM",          price    : "- $45.00",
                        starRatingImg   : "star4",     firstLogoComp    : "Tela",
                        logoComp        : "Gloria_Jeans",   nameComp    : nil,              isNatPosValue: true),
            InvestModel(image           : "CoffeLogo",         title    : "Coffee Logo",
                        subTitle        : "05:10 PM",          price    : "+ $60.00",
                        starRatingImg   : "stars5",    firstLogoComp    : "",
                        logoComp        : "Tela",           nameComp    : "Tela",           isNatPosValue: false),
        ]
    }
}
