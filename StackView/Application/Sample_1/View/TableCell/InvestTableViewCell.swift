//
//  InvestTableViewCell.swift
//  StackView
//
//  Created by Pheayuth's iMac on 22/8/22.
//

import UIKit

class InvestTableViewCell: UITableViewCell {
    
    // IBOutlet
    @IBOutlet weak var starRatingImage  : UIImageView!
    @IBOutlet weak var firstImage       : UIImageView!
    @IBOutlet weak var nameCompanyLabel : UILabel!
    @IBOutlet weak var logoCompanyView  : UIImageView!
    @IBOutlet weak var priceLabel       : UILabel!
    @IBOutlet weak var subTitleLabel    : UILabel!
    @IBOutlet weak var titleLabel       : UILabel!
    @IBOutlet weak var leftImageView    : UIImageView!
    @IBOutlet weak var subStackView: UIStackView!
    
    
    // Function
    func configCell(data: InvestModel){
        
        leftImageView.image     = UIImage(named: data.image ?? "")
        titleLabel.text         = data.title
        subTitleLabel.text      = data.subTitle
        priceLabel.text         = data.price
        starRatingImage.image   = UIImage(named: data.starRatingImg ?? "")
        firstImage.image        = UIImage(named: data.firstLogoComp ?? "")
        logoCompanyView.image   = UIImage(named: data.logoComp ?? "")
        nameCompanyLabel.text   = data.nameComp
        
        if data.firstLogoComp == "" {
            firstImage.isHidden = true
        } else if data.logoComp == "" {
            logoCompanyView.isHidden = true
        } else if data.starRatingImg == "" {
            starRatingImage.isHidden = true
        } else if data.subTitle == "" {
            subTitleLabel.isHidden = true
            subStackView.spacing = 0.0
        } else {
            subStackView.spacing = 5.0
        }
        if data.isNatPosValue == true {
            self.priceLabel.textColor = UIColor.red
        }
        
    }

}

extension UIImageView {
  func setImageColor(color: UIColor) {
    let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
    self.image = templateImage
    self.tintColor = color
  }
}
