//
//  InvestVC.swift
//  StackView
//
//  Created by Pheayuth's iMac on 5/9/22.
//

import UIKit

class InvestVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    // Get data from View Model
    var investData = InvestVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Use getInvest() function
        investData.getInvest()
    }

}

extension InvestVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.investData.investData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "InvestTableViewCell", for: indexPath) as! InvestTableViewCell
        // Configure Cell for Row
        cell.configCell(data: investData.investData[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }

}
