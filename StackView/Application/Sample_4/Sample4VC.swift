//
//  Sample4VC.swift
//  StackView
//
//  Created by Pheayuth's iMac on 2/9/22.
//

import UIKit

class Sample4VC: UIViewController {

    @IBOutlet weak var verticalStack: UIStackView!
    @IBOutlet weak var descLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if view.traitCollection.verticalSizeClass == .compact {
            verticalStack.axis = .horizontal
            self.descLabel.text = "You see the magic, right? Look at the both buttons below 👇🏻👇🏻."
        } else {
            verticalStack.axis = .vertical
            self.descLabel.text = "You can rotate your device to horizontal. You'll see the magic UIStackView does😁"
        }
    }
    
    // This method for cofigure/change vertical stackview to horizontal
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        if view.traitCollection.verticalSizeClass == .compact {
            verticalStack.axis = .horizontal
            self.descLabel.text = "You see the magic, right? Look at the both buttons below 👇🏻👇🏻."
        } else {
            verticalStack.axis = .vertical
            self.descLabel.text = "You can rotate your device to horizontal. You'll see the magic UIStackView does😁"
        }
    }

}
