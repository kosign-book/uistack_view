//
//  Keyboard.swift
//  StackView
//
//  Created by Pheayuth's iMac on 9/9/22.
//

import UIKit

class Keyboard: UIView, NibLoadable {
    
    // 1. Add target for custom keboard
    weak var target: (UIKeyInput & UIResponder)?
    
    @IBOutlet var buttons: [UIButton]!
    
    // 2. Initialize target for keyboard
    init(target: (UIKeyInput & UIResponder)) {
        super.init(frame: .zero)
        self.target = target
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // 3. Create configure func 
    func configure() {
        autoresizingMask = [.flexibleWidth, .flexibleHeight]
        setupFromNib()
        _ = buttons?.map({$0.commonFormat()})
    }
    
    @IBAction func didTapKeyboards(_ sender: UIButton) {
        target?.insertText("\(String(describing: sender.currentTitle ?? ""))" )
    }
    
    @IBAction func deleteButton(_ sender: UIButton) {
        target?.deleteBackward()
    }
    
    @IBAction func okButton(_ sender: UIButton) {
        target?.resignFirstResponder()
    }
    
}


extension UIButton {
    
    func commonFormat() {
        self.titleLabel?.font = .preferredFont(forTextStyle: .title1 )
        self.setTitleColor(.label, for: .normal)
//        self.layer.cornerRadius = 5
        self.accessibilityTraits = [.keyboardKey]
//        self.shadow()
    }

    func shadow(_ radius: CGFloat = 1) {
        self.layer.shadowColor   = UIColor.black.cgColor
        self.layer.shadowOpacity = 1.0
        self.layer.shadowOffset  = CGSize(width: 0.0, height : 1.0)
        self.layer.shadowRadius  = radius
    }
}
