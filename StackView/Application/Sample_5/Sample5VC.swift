//
//  Sample5VC.swift
//  StackView
//
//  Created by Pheayuth's iMac on 5/9/22.
//

import UIKit

class Sample5VC: UIViewController {
    
    @IBOutlet var textF: [UITextField]!
    @IBOutlet weak var textF2: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //
        let _ = textF.map{createKeyboard($0)}
        _ = textF2.map{createKeyboard($0)}
    }
    
    func createKeyboard(_ textField: UITextField) {
//        textField.backgroundColor = .systemGroupedBackground
        textField.clearButtonMode = .whileEditing
        textField.inputView = Keyboard(target: textField)
    }

    

}


extension Sample5VC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let previousText:NSString = textField.text! as NSString
        let updatedText = previousText.replacingCharacters(in: range, with: string)
        print("updatedText > ", updatedText)
        
        //the rest of your code
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}
