//
//  CustomKeyboard.swift
//  StackView
//
//  Created by Pheayuth's iMac on 12/9/22.
//

import UIKit

class CustomKeyboard: UIView, UIInputViewAudioFeedback {

    var target : UITextInput?
    var uiView : UIView?
    
    var enableInputClicksWhenVisible: Bool { return false }

    init(target: UITextInput, uiView: UIView) {
        super.init(frame: .zero)
        self.target = target
        self.uiView = uiView
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
