//
//  EditProfileFixVM.swift
//  StackView
//
//  Created by Pheayuth's iMac on 19/9/22.
//

import Foundation

class EditProfileFixVM {
    var dsf : [EditProfileFixHeaderCellViewModel] = [
        EditProfileFixHeaderCellViewModel(titleText: "Personal",
                                          cell: [.personal(PersonalCellViewModel(titleText: "Username:", descriptionText: "Kresimir")),
                                                 .personal(PersonalCellViewModel(titleText: "Email:", descriptionText: "kresimir@gmail.com")),
                                                 .personal(PersonalCellViewModel(titleText: "Password:", descriptionText: "*********")),
                                                 .personal(PersonalCellViewModel(titleText: "Phone:", descriptionText: "+ 385 99 5656 561")),
                                                 .personal(PersonalCellViewModel(titleText: "Address:", descriptionText: "Great Windmill Street 43")),
                                                 .personal(PersonalCellViewModel(titleText: "City:", descriptionText: "Zagreb")),
                                                 .personal(PersonalCellViewModel(titleText: "Conutry:", descriptionText: "Croatia")),
                                                 .personal(PersonalCellViewModel(titleText: "Gender:", descriptionText: "Male")),
                                                 .personal(PersonalCellViewModel(titleText: "Nickname:", descriptionText: "Soksijada"))],
                                          isExpanded: false),
    ]
}
