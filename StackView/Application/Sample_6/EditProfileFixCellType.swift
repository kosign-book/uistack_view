//
//  EditProfileFixCellType.swift
//  StackView
//
//  Created by Pheayuth's iMac on 14/9/22.
//

import Foundation
import UIKit

enum EditProfileFixCellType {
    case header(EditProfileFixHeaderCellViewModel)
    case personal(PersonalCellViewModel)
    case payMehtod(PayMethodCellViewModel)
    case notification
}

struct EditProfileFixHeaderCellViewModel {
    let titleText   : String?
    let cell        : [EditProfileFixCellType]?
    let isExpanded  : Bool?
}

struct PersonalCellViewModel {
    let titleText : String?
    let descriptionText : String?
}

struct PayMethodCellViewModel {
    let image : UIImage?
}

struct NotificationCellViewModel {
    let titleText : String?
    let isEnable : Bool?
}
