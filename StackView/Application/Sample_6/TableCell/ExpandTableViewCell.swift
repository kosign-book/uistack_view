//
//  ExpandTableViewCell.swift
//  StackView
//
//  Created by Pheayuth's iMac on 19/9/22.
//

import UIKit


class ExpandTableViewCell: UITableViewCell {
    

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var containerStack: UIStackView!
    @IBOutlet weak var imageViewD: UIImageView!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var bottomView: UIView! {
        didSet {
            bottomView.isHidden = true
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configCell(data: SupportResponse.DetailItem) {
        titleLabel.text = data.tileLabel
        descLabel.text = data.desLabel
    }
    
    
}
