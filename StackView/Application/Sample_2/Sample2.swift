//
//  Sample2.swift
//  StackView
//
//  Created by Pheayuth's iMac on 23/8/22.
//

import UIKit

// Review Model
struct Review {
        var title       : String?
        var date        : String?
        var memberSince : String?
        var isExpanded  : Bool
        var description : String?
}

class Sample2: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    // Get Property form Review Model
    var data : [Review] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Table Delegate
        tableView.delegate      = self
        tableView.dataSource    = self
        
        // Register "ReviewTableViewCell" to UITableView
        tableView.register(UINib(nibName: "ReviewTableViewCell", bundle: nil), forCellReuseIdentifier: "ReviewTableViewCell")
           
        // DataSource
        data = [
            Review(title        : "Robert Downey Jr",
                   date         : "12th Feb 2020",
                   memberSince  : "Is a member since 2011",
                   isExpanded   : false,
                   description  : "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text."),
            
            Review(title        : "Cristaino Ronaldo",
                   date         : "5th Feb 2022",
                   memberSince  : "Is a member since 2014",
                   isExpanded   : false,
                   description  : "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text."),
            
            Review(title        : "Robert Downey Jr",
                   date         : "12th Feb 2020",
                   memberSince  : "Is a member since 2011",
                   isExpanded   : false,
                   description  : "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text."),
            
            Review(title        : "Cristaino Ronaldo",
                   date         : "5th Feb 2022",
                   memberSince  : "Is a member since 2014",
                   isExpanded   : false,
                   description  : "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.")
        ]
       
    }
}

extension Sample2: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewTableViewCell", for: indexPath) as! ReviewTableViewCell
        
        // Configure with dataSource to show in TableView
        cell.configCell(self.data[indexPath.row])
        
        // When tapped "See More Button"
        cell.seeMoreDidTap {
            self.data[indexPath.row].isExpanded.toggle()
            tableView.beginUpdates()
            tableView.endUpdates()
        }
        return cell
    }
    
    
}
