//
//  ReviewTableViewCell.swift
//  StackView
//
//  Created by Pheayuth's iMac on 13/9/22.
//

import UIKit

class ReviewTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel       : UILabel!
    @IBOutlet weak var descLabel        : UILabel!
    @IBOutlet weak var dateLabel        : UILabel!
    @IBOutlet weak var seeMoreButton    : UIButton!
    private var isSeeLess = true
    private var seeLessHandler : (() -> Void)? // closure for Handle seeLess/More
    
    
    @IBAction func seeMoreButtonTap() {
        self.isSeeLess.toggle() // toggle is useful for expand text (On/Off)
        // "numberOfLines =" This property controls the maximum number of lines to use in order to fit the label’s text into its bounding rectangle. 👇🏻
        self.descLabel.numberOfLines = self.isSeeLess ? 0 : 3
        self.descLabel.layoutIfNeeded() // Use this method to force the view to update its layout immediately. 
        self.seeMoreButton.setTitle(self.isSeeLess ? "See Less" : "See More", for: .normal)
        self.seeLessHandler?()
    }
    
    func seeMoreDidTap(_ completion: @escaping() -> Void){
        self.seeLessHandler = completion
    }
    
    func configCell(_ dataReview: Review){
        self.titleLabel.text = dataReview.title
        self.descLabel.text  = dataReview.description
        self.dateLabel.text  = dataReview.date

        self.isSeeLess = dataReview.isExpanded
        self.descLabel.numberOfLines = self.isSeeLess ? 0 : 3
        self.seeMoreButton.setTitle(self.isSeeLess ? "See Less" : "See More", for: .normal)
    }
    
}
