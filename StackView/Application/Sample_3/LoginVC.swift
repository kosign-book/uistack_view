//
//  SignUpVC.swift
//  StackView
//
//  Created by Pheayuth's iMac on 22/8/22.
//

import UIKit


class LoginVC: UIViewController {
    
    
    @IBOutlet weak var userNameTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var borderUsername: UIView!
    @IBOutlet weak var borderPassword: UIView!
    
    @IBOutlet weak var googleAction: UIView!
    @IBOutlet weak var facebookAction: UIView!
    @IBOutlet weak var appleAction: UIView!
    
    @IBOutlet weak var registerLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerLabel.attributedText = "New to iThickLogistics? Register".attributedStringWithColorSize(color: UIColor(hexString: "#5856D6"), size: 17)
        
        self.googleAction.addAction(#selector(clickGoogle), target: self)
        self.facebookAction.addAction(#selector(clickFacebook), target: self)
        self.appleAction.addAction(#selector(clickApple), target: self)
        
    }
    
    @objc func clickGoogle(sender:UITapGestureRecognizer) {
        self.callWeb(url: "https://www.google.com/")
    }
    
    @objc func clickFacebook(_ sender: Any) {
        self.callWeb(url: "https://www.facebook.com/")
    }
    
    @objc func clickApple(sender:UIButton) {
        print("Apple Action")
        self.callWeb(url: "https://www.apple.com/")
    }
    
    @IBAction func loginAction(_ sender: Any) {
        // Login Success
        if userNameTF.text == "test1" && passwordTF.text == "12345" {
            let customAlert = RTCustomAlert()
            customAlert.alertTitle = "Login Successfully !!"
            customAlert.alertMessage = "Thank you for using our app."
            customAlert.alertTag = 1
            customAlert.okButtonTitle = " Go ==> "
            customAlert.statusImage = UIImage.init(named: "Success")
            customAlert.isCancelButtonHidden = true
            customAlert.delegate = self
            customAlert.show()
            
            // Login cannot be empty
        } else if userNameTF.text == "" && passwordTF.text == "" {
            
            let customAlert = RTCustomAlert()
            customAlert.alertTitle = "Login Fail !!"
            customAlert.alertMessage = "Username or Password cannot be empty ! \n User: test1\n Pass: 12345"
            customAlert.alertTag = 2
            customAlert.okButtonTitle = "Yes"
            customAlert.statusImage = UIImage.init(named: "fail")
            customAlert.delegate = self
            customAlert.show()

            // Login wrong User../Passw...
        } else {
            
            let customAlert = RTCustomAlert()
            customAlert.alertTitle = "Login Fail !!"
            customAlert.alertMessage = "Please recheck username or password !! \n test1, 12345"
            customAlert.alertTag = 2
            customAlert.okButtonTitle = "Try Again"
            customAlert.statusImage = UIImage.init(named: "fail")
            customAlert.isCancelButtonHidden  =  true
            customAlert.delegate = self
            customAlert.show()
        }
        
    }
    
    
    private func loginAction (){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Sample4", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "Sample4VC") as! Sample4VC
//        nextViewController.modalPresentationStyle = .custom
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    // Func for push to webView
    func callWeb(url: String?) {
        let webKit = self.VC(sbName: "Login", identifier: "WebVC") as! WebVC
        webKit.hidesBottomBarWhenPushed = true
        webKit.urlStr   = url
        self.navigationController?.pushViewController(webKit, animated: true)
    }
    
    func VC(sbName: String, identifier: String) -> UIViewController {
        return UIStoryboard(name: sbName, bundle: nil).instantiateViewController(withIdentifier: identifier)
    }

}

extension LoginVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 1007 {
            borderUsername.borderColor = UIColor(hexString: "#5856D6")
            borderUsername.borderWidth = 1
            borderUsername.cornerRadius = 10
        }
        
        if textField.tag == 1008 {
            borderPassword.borderColor = UIColor(hexString: "#5856D6")
            borderPassword.borderWidth = 1
            borderPassword.cornerRadius = 10
        }
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1007 {
            borderUsername.borderColor = UIColor.white
        }
        
        if textField.tag == 1008 {
            borderPassword.borderColor = UIColor.white
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
}

extension LoginVC : RTCustomAlertDelegate {
    
    func okButtonPressed(_ alert: RTCustomAlert, alertTag: Int) {
        
        if alertTag == 1 {
            print("Single button alert: Ok button pressed")
            self.loginAction()
        } else {
            print("Two button alert: Ok button pressed")
        }
        print(alert.alertTitle)
    }
    
    func cancelButtonPressed(_ alert: RTCustomAlert, alertTag: Int) {
        print("Cancel button pressed")
    }
    
    
}


extension String {
    
    func attributedStringWithColorSize( color: UIColor, size: CGFloat = 12) -> NSAttributedString {
        
        let attributedString = NSMutableAttributedString(string: self)
        
        if self.count < 3 {
            return attributedString
        }
        let range = NSRange(location: self.count - 8, length: 8) // length 8 = "Register"
        
        attributedString.addAttribute(NSAttributedString.Key.font,value: UIFont.systemFont(ofSize: size) , range: range)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
        
        return attributedString
    }
}
