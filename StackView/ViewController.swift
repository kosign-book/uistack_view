//
//  ViewController.swift
//  StackView
//
//  Created by Pheayuth's iMac on 22/8/22.
//

import UIKit

struct ListSamples {
    let listItems   : String?
    let descItem    : String?
}

class ViewController: UIViewController {
   
    @IBOutlet weak var tableView: UITableView!
    
    var listItem : [ListSamples] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        listItem = [
            ListSamples(listItems: "Sample 1", descItem: "Use UIStackView can collect items as group and remove any item without use any constraints…"),
            ListSamples(listItems: "Sample 2", descItem: "Expand text using UIStackView."),
            ListSamples(listItems: "Sample 3", descItem: "All items in UIStackView can equal spacing and sizing without use any constraints"),
            ListSamples(listItems: "Sample 4", descItem: "Can add multiple images in UIStackView resizing with devices by rotating."),
//            ListSamples(listItems: "Sample 5", descItem: "Custom Keyboard with xib file using UIStackView.")
        ]
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    private func sample1() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "InvestSB", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "InvestVC") as! InvestVC
//        nextViewController.modalPresentationStyle = .custom
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    private func sample2() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Sample2", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "Sample2") as! Sample2
        nextViewController.modalPresentationStyle = .custom
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    private func sample3() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Login", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    private func sample4() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Sample4", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "Sample4VC") as! Sample4VC
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    // This func for Custom Keyboard
    private func sample5() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Sample5", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "Sample5VC") as! Sample5VC
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListCell", for: indexPath)
        cell.textLabel?.text = self.listItem[indexPath.row].listItems
        cell.detailTextLabel?.text = self.listItem[indexPath.row].descItem
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            self.sample1()
        } else if indexPath.row == 1 {
            self.sample2()
        } else if indexPath.row == 2{
            self.sample3()
        } else if indexPath.row == 3 {
            self.sample4()
        } else {
            self.sample5()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        60
    }
    
    
}

